// https://www.hackquest.io/syntax/Solana%20%E7%A8%8B%E5%BA%8F%E5%BC%80%E5%8F%91/learn/eb991448-fb28-4f21-b9dc-ded003ae1c2d?menu=learningTrack&learningTrackId=b502eb82-9254-45e7-be22-1f460aafb2cf&menuCourseId=ecf0963e-53d6-41f3-b1e0-10176fe20c53&lessonId=eb991448-fb28-4f21-b9dc-ded003ae1c2d
use borsh::{BorshDeserialize, BorshSerialize};
use solana_program::account_info::{next_account_info, AccountInfo};
use solana_program::entrypoint;
use solana_program::entrypoint::ProgramResult;
use solana_program::msg;
use solana_program::program_error::ProgramError;
use solana_program::pubkey::Pubkey;

entrypoint!(process_instruction);

#[derive(BorshSerialize, BorshDeserialize, Debug)]
pub struct CounterAccount {
    pub count: u64,
}

pub fn process_instruction(
    program_id: &Pubkey,
    accounts: &[AccountInfo],
    _input: &[u8],
) -> ProgramResult {
    msg!("Counter program entrypoint");
    // 获取调用者账号
    let account_iter = &mut accounts.iter();
    let account = next_account_info(account_iter)?;
    if account.owner != program_id {
        msg!("Account is not owned by the program");
        return Err(ProgramError::IncorrectProgramId);
    }
    msg!("Account: {:?}", account);
    // 读取并写入新的值
    let mut counter = CounterAccount::try_from_slice(&account.data.borrow())?;
    counter.count += 1;
    counter.serialize(&mut *account.data.borrow_mut())?;
    Ok(())
}
