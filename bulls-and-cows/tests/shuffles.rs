use rand::seq::SliceRandom;

#[test]
fn test_shuffle() {
    let mut rng = rand::thread_rng();
    let mut nums = vec![1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    nums.shuffle(&mut rng);
    println!("{:?}", nums);
    assert_eq!(nums.len(), 10);
}
