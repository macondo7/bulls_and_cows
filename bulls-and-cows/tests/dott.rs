#[test]
fn test_main() {
    dotenv::dotenv().ok();

    let _ = if let Ok(addr) = std::env::var("GT_MYSQL_ADDR") {
        println!("GT_MYSQL_ADDR: {}", addr);
        Some(addr)
    } else {
        println!("GT_MYSQL_ADDR is empty, ignores test");
        None
    };
}
