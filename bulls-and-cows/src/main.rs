use rand::Rng;
use std::io;

/// [Solana Learning Track - 中文](https://www.hackquest.io/learning-track/b502eb82-9254-45e7-be22-1f460aafb2cf)
fn main() {
    println!("Welcome to Bulls and Cows");
    let secret_number = rand::thread_rng().gen_range(1..11);

    let mut attempts = 0;
    loop {
        println!("Please input a number: ");
        let mut guess = String::new();
        io::stdin()
            .read_line(&mut guess)
            .expect("Oops! Something goes wrong");
        let guess = match guess.trim().parse::<u32>() {
            Ok(num) => num,
            Err(_) => {
                println!("Please input valid number");
                continue;
            }
        };
        attempts += 1;
        if guess < 1 || guess > 10 {
            println!("Please input number between 1 and 10");
            continue;
        }
        match guess.cmp(&secret_number) {
            std::cmp::Ordering::Less => {
                println!("too small!");
                if attempts > 5 {
                    println!("tips: You have tried {attempts} times");
                }
            }
            std::cmp::Ordering::Greater => {
                println!("too big!");
                if attempts > 5 {
                    println!("tips: You have tried {attempts} times");
                }
            }
            std::cmp::Ordering::Equal => {
                println!("Congratulations! You win!");
                println!("tips: You have tried {} times", attempts);
                break;
            }
        }
        if attempts == 10 {
            println!("Sorry! You lose!");
            println!("tips: You have tried {} times", attempts);
            break;
        }
    }
}
